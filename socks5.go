package main

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"runtime"
	"strings"
	"time"
)

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
}

func main() {
	addr := os.Getenv("SOCKS5_LISTEN")
	if addr == "" {
		addr = "0.0.0.0:12000"
	}

	ln, err := net.Listen("tcp", addr)
	if err != nil {
		log.Fatal(err)
	}
	defer ln.Close()

	log.Println("Listen at", addr)

	for {
		client, err := ln.Accept()
		if err != nil {
			log.Println("Accept error:", err)
			continue
		}
		clientAddr := strings.Split(client.RemoteAddr().String(), ":")[0]
		proxy := &Proxy{Client: client, Addr: clientAddr}
		go proxy.Run()
	}
}

type Proxy struct {
	Client net.Conn
	Addr   string
}

const (
	RWTimeout      = 75
	ConnectTimeout = 10
)

func (p *Proxy) Run() {
	defer p.Client.Close()

	buffer := make([]byte, 1024)

	// step 1
	data, err := p.read(buffer)
	if err != nil {
		log.Printf("%s -> step 1 read error: %v\n", p.Addr, err)
		return
	}
	if data[0] != 0x05 {
		log.Printf("%s -> setp 1 version error: version (%#x) is unknown\n", p.Addr, data[0])
		return
	}
	if data[2] != 0x00 {
		log.Printf("%s -> step 1 method error: unsupport method (%#x)\n", p.Addr, data[2])
		err = p.write([]byte{0x05, 0xFF})
		if err != nil {
			log.Printf("%s -> step 1 write error: %v\n", p.Addr, err)
		}
		return
	}
	err = p.write([]byte{0x05, 0x00})
	if err != nil {
		log.Printf("%s -> step 1 write error: %v\n", p.Addr, err)
		return
	}

	// step 2
	data, err = p.read(buffer)
	if err != nil {
		log.Printf("%s -> step 2 read error: %v\n", p.Addr, err)
		return
	}
	if data[0] != 0x05 {
		log.Printf("%s -> step 2 version error: version (%#x) is unknown\n", p.Addr, data[0])
		return
	}
	if data[1] != 0x01 {
		log.Printf("%s -> step 2 cmd error: unsupported cmd (%#x)\n", p.Addr, data[1])
		err = p.write([]byte{0x05, 0x07})
		if err != nil {
			log.Printf("%s -> step 2 write error: %v\n", p.Addr, err)
		}
		return
	}
	host, port, err := p.getHostPort(data[3:])
	if err != nil {
		log.Printf("%s -> step 2 get host and port error: %v\n", p.Addr, err)
		return
	}
	//log.Printf("dst: %s:%d\n", host, port)

	//step 3
	p.tcpRelay(host, port)

}

func (p *Proxy) tcpRelay(host string, port int16) {
	dst := fmt.Sprintf("%s:%d", host, port)
	server, err := net.DialTimeout("tcp", dst, time.Duration(ConnectTimeout)*time.Second)
	if err != nil {
		log.Printf("%s Connected to %s error: %v\n", p.Addr, dst, err)
		return
	}
	defer server.Close()

	log.Printf("%s Connected to %s\n", p.Addr, dst)

	err = p.write([]byte{0x05, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x50})
	if err != nil {
		log.Printf("%s Write error: %v\n", p.Addr, err)
		return
	}

	done := make(chan bool)
	server.SetDeadline(time.Now().Add(time.Duration(RWTimeout) * time.Second))
	p.Client.SetDeadline(time.Now().Add(time.Duration(RWTimeout) * time.Second))
	go func() {
		io.Copy(server, p.Client)
		//_, err := io.Copy(server, p.Client)
		//if err != nil {
		//	log.Printf("copy %s to %s error: %v\n", p.Addr, dst, err)
		//}
		done <- true
	}()

	io.Copy(p.Client, server)
	//_, err = io.Copy(p.Client, server)
	//if err != nil {
	//	log.Printf("copy %s to %s error: %v\n", dst, p.Addr, err)
	//}
	<-done
}

func (p *Proxy) getHostPort(b []byte) (host string, port int16, err error) {
	switch b[0] {
	case 0x01: //ipv4
		host = net.IPv4(b[1], b[2], b[3], b[4]).String()
		err = binary.Read(bytes.NewReader(b[5:7]), binary.BigEndian, &port)
	case 0x03: //domain
		hostLen := int(b[1])
		host = string(b[2 : 2+hostLen])
		err = binary.Read(bytes.NewReader(b[2+hostLen:2+hostLen+2]), binary.BigEndian, &port)
	default:
		err = errors.New(fmt.Sprintf("unsupported address type (%#x)\n", b[0]))
		p.Client.Write([]byte{0x05, 0x08})
	}
	return
}

func (p *Proxy) read(b []byte) (data []byte, err error) {
	err = p.Client.SetReadDeadline(time.Now().Add(time.Duration(RWTimeout) * time.Second))
	if err != nil {
		return
	}

	n, err := p.Client.Read(b)
	if err != nil {
		return
	}
	data = b[:n]
	return
}

func (p *Proxy) write(b []byte) (err error) {
	err = p.Client.SetWriteDeadline(time.Now().Add(time.Duration(RWTimeout) * time.Second))
	if err != nil {
		return
	}
	_, err = p.Client.Write(b)
	return
}
